import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'practica';

  showButton(event): void {
    alert(event.name);
  }

  constructor() { }

  principal: Array<any> = [{
    text: "Faq",
    link: "#"
  },
  {
    text: "Contact",
    link: "#"
  },
  {
    text: "About",
    link: "#"
  },
  {
    text: "Services",
    link: "/services"
  }, {
    text: "Home",
    link: "#"
  },]

  principal2: Array<any> = [{
    title: "Modern Landing Page",
    button: "GET STARTED"
  }]
}
