import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule, Route } from '@angular/router';
import { PrincipalComponent } from './pages/principal/principal.component';
import { LogoComponent } from './components/logo/logo.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ContentComponent } from './components/content/content.component';
import { ServicesComponent } from './pages/services/services.component';

const routes: Route[] = [
  {path: '', component: PrincipalComponent},
  {path: 'services', component: ServicesComponent}
] 

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    LogoComponent,
    NavbarComponent,
    ContentComponent,
    ServicesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
