import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  @Input() title: String;
  @Input() button: String;
  @Input() text: String;

  @Output() ButtonAction = new EventEmitter();

  public name: String;

  constructor() {
    this.name = "Iniciar";

  }

  lanzar(event) {
    this.ButtonAction.emit({ name: this.name });
  }


  ngOnInit(): void {
  }

}
