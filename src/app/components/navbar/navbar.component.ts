import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input() text: String;
  @Input() link: String;

  @Output() ButtonAction = new EventEmitter();

  public name: String;

  constructor() {
    this.name = "Este boton abrira una nueva página";

  }

  lanzar(event) {
    this.ButtonAction.emit({ name: this.name });
  }


  ngOnInit(): void {
  }

}
